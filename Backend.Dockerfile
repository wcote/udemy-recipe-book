FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env

WORKDIR /recipe-book-backend
COPY udemy-recipe-book-backend/ .

RUN dotnet publish backend.sln -c Debug -o builded-artifact

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2

WORKDIR /recipe-book-backend
COPY --from=build-env /recipe-book-backend/builded-artifact .

ENTRYPOINT ["dotnet", "udemy-recipe-book-backend.dll"]