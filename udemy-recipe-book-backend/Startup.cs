﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Udemy.Config;
using Udemy.Middleware;

namespace Udemy
{
    public class Startup
    {
        private const string CORS_POLICY_NAME = "CorsPolicyName";
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Register(services);
            SetupCrossOrigins(services);
            services.AddMvcCore()
                .AddJsonFormatters(x => x.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
            
            SetupDatabase(services);
        }
        
        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMiddleware<ExceptionHandler>();
            app.UseCors(CORS_POLICY_NAME);
            app.UseMvc();
        }
        
        private void Register(IServiceCollection services) =>
                    new DependencyInjectionRegistry().RegisterDependencies(services);
        
        private void SetupDatabase(IServiceCollection services)
        {
            services.AddDbContext<BackendContext>(opt =>
                opt.UseNpgsql(Configuration.GetConnectionString("RecipeBookConnection")));
        }
        
        private static void SetupCrossOrigins(IServiceCollection services)
        {
            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.AllowAnyHeader();
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyOrigin();

            services.AddCors(options => { options.AddPolicy(CORS_POLICY_NAME, corsBuilder.Build()); });
        }
    }
}
