using System.Net;

namespace Udemy.Utils.Exceptions
{
    public class BadRequestException : BaseException
    {
        public BadRequestException(string message) : base(message, HttpStatusCode.BadRequest)
        {
        }
    }
}