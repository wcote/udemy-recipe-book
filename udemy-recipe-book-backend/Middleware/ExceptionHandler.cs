using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Udemy.Utils.Exceptions;

namespace Udemy.Middleware
{
    public class ExceptionHandler
    {
        private readonly RequestDelegate _next;

        public ExceptionHandler(RequestDelegate next)
        {
            _next = next;
        }
        
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (BaseException baseException)
            {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int) baseException.StatusCode;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(baseException.Message));
            }
        }
    }
}