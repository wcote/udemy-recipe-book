using System.Collections.Generic;
using System.Threading.Tasks;
using Udemy.Model;
using Udemy.Services.Application.Interfaces;
using Udemy.Utils.Exceptions;

namespace Udemy.Services.Application
{
    public class IngredientService : BaseService, IIngredientService
    {
        private const string INGREDIENT_ALREADY_EXIST = "Ingredient already exists";
        
        public IngredientService(IDbService dbService) : base(dbService)
        {
        }

        public async Task<IEnumerable<Ingredient>> GetAllAsync()
        {
            using (var uow = _dbService.UnitOfWork)
            {
                return await uow.IngredientRepository.GetAllAsync();
            }
        }

        public async Task CreateAsync(Ingredient ingredient)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                ValidateIngredientDoesntExist(uow, ingredient.Name);
                await uow.IngredientRepository.AddAsync(ingredient);
                await uow.CompleteAsync();
            }
        }
        
        private void ValidateIngredientDoesntExist(IUnitOfWork uow, string name)
        {
            if (uow.IngredientRepository.GetIngredientByNameAsync(name).Result != null)
            {
                throw new BadRequestException(INGREDIENT_ALREADY_EXIST);
            }
        }
    }
}