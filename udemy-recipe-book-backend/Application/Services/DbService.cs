using Microsoft.EntityFrameworkCore;
using Udemy.Services.Application.Interfaces;
using Udemy.Config;

namespace Udemy.Services.Application
{
    public class DbService : IDbService
    {
        private readonly DbContextOptions _options;

        public DbService()
        {
            var optionsBuilder = new DbContextOptionsBuilder();
            optionsBuilder.UseNpgsql(BackendConfig.CONN_STRING);
            _options = optionsBuilder.Options;
        }

        public BackendContext GetDbContext()
        {
            var context = new BackendContext(_options);
            context.Database.Migrate();

            return context;
        }

        public IUnitOfWork UnitOfWork =>
            new UnitOfWork(GetDbContext());
    }
}