using Udemy.Services.Application.Interfaces;

namespace Udemy.Services.Application
{
    public class BaseService
    {
        protected IDbService _dbService;

        public BaseService(IDbService dbService)
        {
            _dbService = dbService;
        }
    }
}