using System;
using System.Threading.Tasks;
using Udemy.Application.Repositories;
using Udemy.Application.Repositories.Interfaces;
using Udemy.Config;
using Udemy.Services.Application.Interfaces;

namespace Udemy.Services.Application
{  
    public class UnitOfWork : IUnitOfWork
    {
        private static BackendContext _context;
        public IIngredientRepository IngredientRepository { get; }
        public IRecipeRepository RecipeRepository { get; }
        private bool disposed;
        
        public UnitOfWork(BackendContext context)
        {
            _context = context;
            IngredientRepository = new IngredientRepository(_context);
            RecipeRepository = new RecipeRepository(_context);
        }

        public Task<int> CompleteAsync() =>
            _context.SaveChangesAsync();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed)
                if (disposing)
                    _context.Dispose();

            disposed = true;
        }
    }
}