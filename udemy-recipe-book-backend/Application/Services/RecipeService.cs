using System.Collections.Generic;
using System.Threading.Tasks;
using Udemy.Model;
using Udemy.Services.Application.Interfaces;
using Udemy.Utils.Exceptions;

namespace Udemy.Services.Application
{
    public class RecipeService : BaseService, IRecipeService
    {
        private const string RECIPE_ALREADY_EXIST = "Recipe already exists";
        
        public RecipeService(IDbService dbService) : base(dbService)
        {
        }
        
        public async Task<IEnumerable<Recipe>> GetAllAsync()
        {
            using (var uow = _dbService.UnitOfWork)
            {
                return await uow.RecipeRepository.GetAllRecipeAsync();
            }
        }
        
        public async Task CreateAsync(Recipe[] recipes)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                foreach (Recipe recipe in recipes)
                {
                    ValidateRecipeDoesntExist(uow, recipe.Name);
                    await uow.RecipeRepository.AddAsync(recipe);
                    await uow.CompleteAsync();   
                }
            }
        }
        
        private void ValidateRecipeDoesntExist(IUnitOfWork uow, string name)
        {
            if (uow.IngredientRepository.GetIngredientByNameAsync(name).Result != null)
            {
                throw new BadRequestException(RECIPE_ALREADY_EXIST);
            }
        }
    }
}