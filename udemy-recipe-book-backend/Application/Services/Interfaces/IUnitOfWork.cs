using System;
using System.Threading.Tasks;
using Udemy.Application.Repositories.Interfaces;

namespace Udemy.Services.Application.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IIngredientRepository IngredientRepository { get; }
        IRecipeRepository RecipeRepository { get; }

        Task<int> CompleteAsync();
    }
}