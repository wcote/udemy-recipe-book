using System.Collections.Generic;
using System.Threading.Tasks;
using Udemy.Model;

namespace Udemy.Services.Application.Interfaces
{
    public interface IRecipeService
    {
        Task<IEnumerable<Recipe>> GetAllAsync();
        Task CreateAsync(Recipe[] recipes);
    }
}