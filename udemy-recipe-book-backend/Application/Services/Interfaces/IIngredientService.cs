using System.Collections.Generic;
using System.Threading.Tasks;
using Udemy.Model;

namespace Udemy.Services.Application.Interfaces
{
    public interface IIngredientService
    {
        Task<IEnumerable<Ingredient>> GetAllAsync();
        Task CreateAsync(Ingredient ingredient);
    }
}