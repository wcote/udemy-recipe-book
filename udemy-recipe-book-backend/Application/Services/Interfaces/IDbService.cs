using Udemy.Config;

namespace Udemy.Services.Application.Interfaces
{
    public interface IDbService
    {
        IUnitOfWork UnitOfWork { get; }

        BackendContext GetDbContext();
    }
}