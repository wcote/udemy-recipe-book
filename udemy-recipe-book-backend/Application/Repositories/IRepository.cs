using System.Collections.Generic;
using System.Threading.Tasks;

namespace Udemy.Application.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task AddAsync(T obj);

        void Remove(T obj);

        void Update(T obj);
    }
}