using System.Threading.Tasks;
using Udemy.Model;

namespace Udemy.Application.Repositories.Interfaces
{
    public interface IIngredientRepository : IRepository<Ingredient>
    {
        Task<Ingredient> GetIngredientByNameAsync(string ingredientName);
    }
}