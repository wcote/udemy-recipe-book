using System.Collections.Generic;
using System.Threading.Tasks;
using Udemy.Model;

namespace Udemy.Application.Repositories.Interfaces
{
    public interface IRecipeRepository : IRepository<Recipe>
    {
        Task<Recipe> GetRecipeByNameAsync(string recipeName);
        Task<IEnumerable<Recipe>> GetAllRecipeAsync();
    }
}