using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udemy.Application.Repositories.Interfaces;
using Udemy.Model;

namespace Udemy.Application.Repositories
{
    public class RecipeRepository : Repository<Recipe>, IRecipeRepository
    {
        public RecipeRepository(DbContext context) : base(context)
        {
        }

        public async Task<Recipe> GetRecipeByNameAsync(string recipeName) =>
            await _set
                .Include(x => x.Ingredients)
                .FirstOrDefaultAsync(recipe => recipe.Name == recipeName);
        
        public async Task<IEnumerable<Recipe>> GetAllRecipeAsync() =>
            await _set
                .Include(x => x.Ingredients)
                .ToListAsync();
    }
}