using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Udemy.Application.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected DbSet<T> _set;
        
        public Repository(DbContext context)
        {
            _set = context.Set<T>();
        }
        
        public async Task<IEnumerable<T>> GetAllAsync() =>
            await _set.ToListAsync();

        public async Task AddAsync(T obj) =>
            await _set.AddAsync(obj);

        public void Remove(T obj) =>
            _set.Remove(obj);

        public void Update(T obj) =>
            _set.Update(obj);
    }
}