using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udemy.Application.Repositories.Interfaces;
using Udemy.Model;

namespace Udemy.Application.Repositories
{
    public class IngredientRepository : Repository<Ingredient>, IIngredientRepository
    {
        public IngredientRepository(DbContext context) : base(context)
        {
        }

        public async Task<Ingredient> GetIngredientByNameAsync(string ingredientName) =>
            await _set.FirstOrDefaultAsync(ingredient => ingredient.Name == ingredientName);
    }
}