using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Udemy.Config;
using Udemy.Model;
using Udemy.Services.Application.Interfaces;

namespace Udemy.Controllers
{
    [Produces("application/json")]
    [Route(BackendConfig.BASE_API_PATH + "/[controller]")]
    public class RecipeController : Controller
    {
        private readonly IRecipeService _recipeService;
        
        public RecipeController(IRecipeService recipeService)
        {
            _recipeService = recipeService;
        }

        /// <summary>
        /// GET .../api/recipe/all
        /// </summary>
        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _recipeService.GetAllAsync());
        }
        
        /// <summary>
        /// POST .../api/recipe
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Recipe[] recipes)
        {
            await _recipeService.CreateAsync(recipes);
            return Ok();
        }
    }
}