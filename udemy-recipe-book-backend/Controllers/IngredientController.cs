using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Udemy.Config;
using Udemy.Model;
using Udemy.Services.Application.Interfaces;

namespace Udemy.Controllers
{
    [Produces("application/json")]
    [Route(BackendConfig.BASE_API_PATH + "/[controller]")]
    public class IngredientController : Controller
    {
        private readonly IIngredientService _ingredientService;
        
        public IngredientController(IIngredientService ingredientService)
        {
            _ingredientService = ingredientService;
        }

        /// <summary>
        /// GET .../api/ingredient/all
        /// </summary>
        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _ingredientService.GetAllAsync());
        }
        
        /// <summary>
        /// POST .../api/ingredient
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Ingredient ingredient)
        {
            await _ingredientService.CreateAsync(ingredient);
            return Ok();
        }
    }
}