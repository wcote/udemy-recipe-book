using Microsoft.Extensions.DependencyInjection;
using Udemy.Services.Application;
using Udemy.Services.Application.Interfaces;
using Udemy.Config.Interfaces;

namespace Udemy.Config
{
    public class DependencyInjectionRegistry
    {
        public void RegisterDependencies(IServiceCollection services)
        {
            services.AddSingleton<IBackendConfig, BackendConfig>();
            services.AddSingleton<IDbService, DbService>();
            services.AddSingleton<IIngredientService, IngredientService>();
            services.AddSingleton<IRecipeService, RecipeService>();
        }
    }
}