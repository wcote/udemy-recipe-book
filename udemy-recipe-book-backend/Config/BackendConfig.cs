using Udemy.Config.Interfaces;

namespace Udemy.Config
{
    public class BackendConfig : IBackendConfig
    {
        public const string BASE_API_PATH = "api";
        public const string CONN_STRING = "Host=recipe-book-db;Port=5432;Username=postgres;Password=admin;Database=recipe-book;";
    }
}