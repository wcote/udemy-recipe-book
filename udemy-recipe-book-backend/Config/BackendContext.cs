using Microsoft.EntityFrameworkCore;
using Udemy.Model;

namespace Udemy.Config
{
    public class BackendContext : DbContext
    {
        public BackendContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ingredient>()
                .HasKey(x => x.ID);
            
            modelBuilder.Entity<Ingredient>()
                .Property(x => x.ID)
                .ValueGeneratedOnAdd();
            
            modelBuilder.Entity<Recipe>()
                .HasKey(x => x.ID);

            modelBuilder.Entity<Recipe>()
                .Property(x => x.ID)
                .ValueGeneratedOnAdd();
        }
    }
}