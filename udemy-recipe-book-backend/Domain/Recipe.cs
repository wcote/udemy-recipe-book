using System.Collections.Generic;

namespace Udemy.Model
{
    public class Recipe
    {
        public long ID { get; set; } = 0;
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public List<Ingredient> Ingredients { get; set; } = new List<Ingredient>();
    }
}