namespace Udemy.Model
{
    public class Ingredient
    {
        public long ID { get; set; } = 0;
        public string Name { get; set; }
        public int Amount { get; set; }
    }
}