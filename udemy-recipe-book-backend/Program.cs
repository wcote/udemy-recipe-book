﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Udemy
{
    public class Program
    {
        public static void Main(string[] args) =>
            BuildWebHost(args).Run();

        private static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://0.0.0.0:9090")
                .UseStartup<Startup>()
                .Build();
    }
}
