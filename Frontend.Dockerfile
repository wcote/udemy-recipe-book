FROM node:11.10.1-alpine as build-env

WORKDIR /recipe-book-frontend
COPY udemy-recipe-book-frontend/package.json udemy-recipe-book-frontend/package-lock.json ./

## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm i \
    && mkdir /ng-app \
    && cp -R ./node_modules ./ng-app

WORKDIR /ng-app

COPY udemy-recipe-book-frontend/ .

## Build the angular app in production mode and store the artifacts in dist folder
RUN $(npm bin)/ng build --aot --prod

FROM nginx:1.16.0-alpine

COPY ./configuration/nginx-frontend-docker.conf /etc/nginx/conf.d/default.conf

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

COPY --from=build-env /ng-app/dist/udemy-app /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]